  const puppeteer = require( 'puppeteer' );
  const cron = require( "node-cron" );

  async function on() {
    const browser = await puppeteer.launch( {
      headless: true
    } );
    const page = await browser.newPage();
    await page.goto( 'http://192.168.10.1/' );

    await page.waitForSelector( '#Frm_Username' );
    await page.type( '#Frm_Username', 'admin' );

    await page.waitForSelector( '#Frm_Password' );
    await page.type( '#Frm_Password', 'E9B77' );

    await page.click( '#LoginId' );
    
    await page.waitForTimeout( 1000 );


    await page.waitForSelector( 'html body div#page_container div#page_content div#commPageContainer.commPageContainer div#commLeft div#class2Menu.class2Menu ul#class2MenuItem li a#mmWLANCfg.class1MenuClass' );
    await page.click( 'html body div#page_container div#page_content div#commPageContainer.commPageContainer div#commLeft div#class2Menu.class2Menu ul#class2MenuItem li a#mmWLANCfg.class1MenuClass' );

    await page.waitForTimeout( 1000 );


    await page.waitForSelector( '#ssmWlanMACFilter' );
    await page.click( '#ssmWlanMACFilter' );

    await page.waitForTimeout( 1000 );


    await page.waitForSelector( '#ACLPolicyRadio0_0' );
    await page.click( '#ACLPolicyRadio0_0' );

    await page.waitForTimeout( 1000 );

    // Close the browser
    await browser.close();

    console.log( "Done....!" );
  }

  async function off() {
    const browser = await puppeteer.launch( {
      headless: true
    } );
    const page = await browser.newPage();
    // Navigate to the page where the login form is located
    await page.goto( 'http://192.168.10.1/' );

    // Wait for the username input field to be visible
    await page.waitForSelector( '#Frm_Username' );
    await page.type( '#Frm_Username', 'admin' );



    // Wait for the password input field to be visible
    await page.waitForSelector( '#Frm_Password' );
    await page.type( '#Frm_Password', 'E9B77' );


    // Click on the login button
    await page.click( '#LoginId' );


    await page.waitForTimeout( 1000 );

      await page.waitForSelector( 'html body div#page_container div#page_content div#commPageContainer.commPageContainer div#commLeft div#class2Menu.class2Menu ul#class2MenuItem li a#mmWLANCfg.class1MenuClass' );
    await page.click( 'html body div#page_container div#page_content div#commPageContainer.commPageContainer div#commLeft div#class2Menu.class2Menu ul#class2MenuItem li a#mmWLANCfg.class1MenuClass' );

      await page.waitForTimeout( 1000 );


      await page.waitForSelector( '#ssmWlanMACFilter' );
    await page.click( '#ssmWlanMACFilter' );

      await page.waitForTimeout( 1000 );

    await page.waitForSelector( '#ACLPolicyRadio1_0' );
    await page.click( '#ACLPolicyRadio1_0' );

    await page.waitForTimeout( 1000 );

      await browser.close();

    console.log( "Done....!" );
  }

  // Schedule the main() function to run at 8 PM every day
  cron.schedule( "0 19 * * *", function () {
    console.log( "Running the task at 8 PM every day" );
    off();
  } );

   // Schedule the main() function to run at 8 PM every day
   cron.schedule( "0 20 * * *", function () {
     console.log( "Running the task at 8 PM every day" );
     off();
   } );



  cron.schedule( "0 6 * * *", function () {
    console.log( "Running the task at 3 AM every day" );
    on();
  } );

  cron.schedule( "0 5 * * *", function () {
    console.log( "Running the task at 3 AM every day" );
    on();
  } );
